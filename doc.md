Help on package tkadw:

NAME
    tkadw - # 自0.5.0版本开始，正式重做

PACKAGE CONTENTS
    __main__
    adwite
    windows (package)

SUBMODULES
    base
    button
    drawengine
    drawwidget
    entry
    frame
    gradient
    icon
    label
    layout
    roundrect
    separator
    style
    text
    themebuilder
    widget
    window

DATA
    ARC = 'arc'
    CENTRAL = 'central'
    CIRCULAR = 'circular'
    DARK = 'dark'
    HORIZONTAL = 'horizontal'
    ICONDARK = 'C://Users//ADMINI~1//AppData//Local//Temp//tmp03cjqwfi'
    ICONLIGHT = 'C://Users//ADMINI~1//AppData//Local//Temp//tmpil0j6sjp'
    LIGHT = 'light'
    VERTICAL = 'vertical'
    X = 'x'
    Y = 'y'

VERSION
    0.5.0a4

FILE
    f:\tkadw\tkadw\__init__.py


