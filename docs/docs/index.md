# 主页

## 简介
欢迎来到 tkadw 的文档网站！tkadw 是 tkinter 的界面库，旨在于制作最漂亮的界面，实现最简洁的代码

???+ note "特征"

    !!! note ""
  
        - [x] 已实现
        - [ ] 未实现 & 准备中
  
    - [x] 更加现代化组件，圆角边框
    - [x] 高级的绘画引擎，用来制作更漂亮的界面
    - [ ] 窗口样式 AdwWindowManager
        * [x] Windows平台 使用`pywinstyles`模块 支持`light` `dark` `aero` `acrylic` `mica`等等
        * [x] MacOS平台 支持`light` `dark`
        * [ ] Linux平台 无
    - [ ] 主题
        * [x] 主题化组件
        * [x] Ant Design主题
        * [ ] Gtk主题
        * [ ] Material主题
        * [x] Windows11主题
    - [ ] 丰富的组件
        * [x] 按钮 AdwButton
        * [x] 圆形按钮 AdwCircularButton
        * [ ] 多选按钮 AdwCheckButton
        * [x] 窗口关闭按钮 AdwCloseButton
        * [x] 输入框 AdwEntry
        * [x] 容器 & 框架 AdwFrame
        * [x] 标签 AdwLabel
        * [ ] 列表 AdwListBox
        * [ ] 多文档窗口
            + [x] Windows平台 AdwWindowsMDI
            + [ ] 跨平台 AdwMDI
        * [ ] 菜单栏 AdwMenuBar
        * [ ] 单选按钮 AdwRadioButton
        * [x] 分割条 AdwSeparator
        * [x] 拖拽大小手柄 AdwSizegrip
        * [x] 多文本输入框 AdwText
        * [x] 标题栏 AdwTitleBar
        * [ ] 切换按钮 AdwToggleButton

## 关于文档
本网站是使用`mkdocs`构建的，使用`mkdocs-material`主题

### 主题配置
```css
[data-md-color-scheme="default"] {
  --md-primary-bg-color:                   #f4f4f4;
  --md-primary-fg-color:                    #005a9e;
  --md-primary-fg-color--light:         #005a9e;
  --md-primary-fg-color--dark:        #005a9e;
  --md-accent-fg-color:                      #005a9e;
  --md-accent-bg-color:                     #002642;
}
[data-md-color-scheme="slate"] {
  --md-primary-bg-color:                   #242424;
  --md-primary-fg-color:                    #76b9ed;
  --md-primary-fg-color--light:         #76b9ed;
  --md-primary-fg-color--dark:        #76b9ed;
  --md-accent-fg-color:                      #76b9ed;
  --md-accent-bg-color:                     #76b9ed;
}
```


## 作者
> [XiangQinxi](mailto:XiangQinxi@outlook.com)
> 
>[![b-2bc2b0d4b9c912f9babb9f628c6e5164.jpg](https://i.postimg.cc/y82JhtQd/b-2bc2b0d4b9c912f9babb9f628c6e5164.jpg)](https://postimg.cc/K1rvxqyS)
>
> 头像是`FNAF`中的`小左(Left)`

*[tkadw]: 全称tkadwite
*[tkinter]: Python Tcl/Tk 内置库
