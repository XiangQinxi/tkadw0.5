# 下载 & 安装

!!! note "笔记"
    
    当前tkadw0.5.x还处于开发中，未发布正式版只有预览版，所以需要使用`--pre`

```shell
pip install tkadw -U --pre
```
```
Looking in indexes: https://pypi.tuna.tsinghua.edu.cn/simple
Collecting tkadw
  Downloading https://pypi.tuna.tsinghua.edu.cn/packages/b0/bc/bb97184d0042fce17
235196ddbd5caafa8d9b82b57b0a02b7a7c1672cd1b/tkadw-0.5.0a8-py3-none-any.whl (151
kB)
     ---------------------------------------- 0.0/152.0 kB ? eta -:--:--
     ------- ------------------------------- 30.7/152.0 kB 1.4 MB/s eta 0:00:01
     -------------------------------------- 152.0/152.0 kB 1.1 MB/s eta 0:00:00
Requirement already satisfied: easydict<2.0,>=1.11 in \.venv\lib\site-pa
ckages (from tkadw) (1.11)
Requirement already satisfied: tkinter-dndr<0.0.6,>=0.0.5 in \.venv\lib\
site-packages (from tkadw) (0.0.5)
Requirement already satisfied: tkinterflow<0.0.6,>=0.0.5 in \.venv\lib\s
ite-packages (from tkadw) (0.0.5)
Installing collected packages: tkadw
Successfully installed tkadw-0.5.0a8
```