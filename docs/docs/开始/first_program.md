# 第一个程序
在编码之前，你可以先预览一下 tkadw 的效果

```shell
python -m tkadw
```

## 第一个窗口

```python
from tkadw import *

# 初始化主题
theme = nametotheme(
    "ant"
)

root = AdwTMainWindow()  # 初始化窗口
root.theme(theme, "dark")  # 设置全局主题

## ---------
## 将你的程序放在这里
## ---------

root.run()  # 运行窗口
```

![展示效果](first_program_01.png)

这确实太单调了，什么也没有，是时候加些组件了
```python
button = AdwTButton(root, text="Hello World")
button.pack(padx=10, pady=10)
```

```python
from tkadw import *

# 初始化主题
theme = nametotheme(
    "ant"
)

root = AdwTMainWindow()  # 初始化窗口
root.theme(theme, "dark")  # 设置全局主题

## ---------
## 将你的程序放在这里

button = AdwTButton(root, text="Hello World")
button.pack(padx=10, pady=10)

## ---------

root.run()  # 运行窗口
```

![展示效果](first_program_02.png)

这样就加上一个按钮

如果你觉得这个主题不好看，可以尝试换一个，暂时只有`ant` `win11`这两种主题

```python
theme = nametotheme(
    "win11"
)
```

```python
from tkadw import *

# 初始化主题
theme = nametotheme(
    "win11"
)

root = AdwTMainWindow()  # 初始化窗口
root.theme(theme, "dark")  # 设置全局主题

## ---------
## 将你的程序放在这里

button = AdwTButton(root, text="Hello World")
button.pack(padx=10, pady=10)

## ---------

root.run()  # 运行窗口
```

![展示效果](first_program_03.png)