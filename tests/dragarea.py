from tkadw import *

theme = nametotheme(
    "win11"
)

root = AdwTMainWindow()
root.theme(theme, "dark")
root.custom()
root.geometry("500x340")

menubar = create_root_themed_menubar()
AdwWindowDragArea(root).bind(menubar)

button = AdwTButton()
AdwWidgetDragArea(button).bind()
button.place(x=100, y=100)

root.mainloop()
