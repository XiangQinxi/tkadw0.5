import numpy as np
from tkinter import *

root = Tk()

canvas = Canvas(root, width=400, height=400)
canvas.pack()

# 原始线条的数据点
points = np.array([[50, 50], [150, 100], [250, 50], [350, 100]])

# 使用插值方法获取连续曲线的数据点
interp_points = np.linspace(0, 1, 100)
x = np.interp(interp_points, [0, 1], points[:, 0])
y = np.interp(interp_points, [0, 1], points[:, 1])

# 绘制插值后的线条
for i in range(len(x) - 1):
    canvas.create_line(x[i], y[i], x[i + 1], y[i + 1])

root.mainloop()
