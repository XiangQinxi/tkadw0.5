from tkinter import *

root = Tk()

grip = Button(root, cursor="sizing")
grip.pack(anchor="se", side="bottom")

width, height, startx, starty = 0, 0, 0, 0


def click(event: Event):
    global startx, starty, width, height
    width, height = root.winfo_width(), root.winfo_height()
    startx, starty = event.x_root, event.y_root
    print("width ->", width, "height ->", height)
    print("startx ->", startx, "starty ->", starty)


def move(event: Event):
    global startx, starty, width, height
    new_width = width + (event.x_root - startx)
    new_height = height + (event.y_root - starty)

    if new_width < 0:
        new_width = 0
    if new_height < 0:
        new_height = 0
    root.geometry(f"{new_width}x{new_height}")


grip.bind("<Button-1>", click)
grip.bind("<B1-Motion>", move)

root.mainloop()
