from tkadw import *
from pywinstyles.py_win_style import get_accent_color
# 初始化主题

print(get_accent_color())

theme = nametotheme(
    "win11"
)

root = AdwTMainWindow()  # 初始化窗口
root.theme(theme, "dark")  # 设置全局主题

## ---------
## 将你的程序放在这里

button = AdwTButton(root, text="Hello World")
button.pack(padx=10, pady=10)

## ---------

root.run()  # 运行窗口