from tkadw import *
from tkinter import *

theme = nametotheme(
    "win11"
)
print(theme.fullpath)

window = AdwTMainWindow()
window.theme(theme, "dark")
window.geometry("540x380")

titlebar = customwindow(window)

toplevel = AdwTWindow()
toplevel.titlebar = customwindow(toplevel)


menubar = create_root_themed_menubar()
bind_window_drag(None, menubar)

mainframe = AdwTFrame(window)

AdwTButton(mainframe, text="AdwTButton", command=lambda: print("Button")).pack(fill="x", padx=10, pady=10)

AdwTEntry(mainframe).pack(fill="x", padx=10, pady=10)

AdwTDivider(mainframe).pack(fill="x", padx=10, pady=10)

AdwTCircularButton(mainframe, text="AdwTCircularButton").pack(padx=10, pady=10)

mainframe.pack(fill="both", expand="yes", padx=10, pady=10)

sizegrip = AdwTSizegrip(window)
sizegrip.pack(side="bottom", anchor="se", padx=10, pady=5)

window.mainloop()
