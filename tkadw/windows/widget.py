from .layout import AdwLayout
from .base import AdwBase


class AdwWidget(AdwLayout, AdwBase):
    id = "widget"
