import contextlib
import os

local = os.path.abspath(os.path.dirname(__file__))


@contextlib.contextmanager
def chdir(target: str):
    """Context-managed chdir, original implementation by GitHub @Akuli"""
    current = os.getcwd()
    try:
        os.chdir(target)
        yield
    finally:
        os.chdir(current)


def main():
    with chdir(os.path.join(local, "docs")):
        from os import system

        system("make clean")
        system("make html")
        system("make epub")
        system("make text")


if __name__ == '__main__':
    main()
