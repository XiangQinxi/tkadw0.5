from os import system


def main():
    from build_docs import main as build_docs
    system("poetry publish --build")


if __name__ == '__main__':
    main()
